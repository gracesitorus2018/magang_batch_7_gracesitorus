/*1.Jumlah komplain setiap bulan*/

SELECT DATE_FORMAT(date_received, "%m") AS MONTH, COUNT(complaint_id) AS jumlah 
FROM complaints
GROUP BY DATE_FORMAT(date_received, "%m")
ORDER BY DATE_FORMAT(date_received, "%m")

/*Komplain yang memiliki tags ‘Older American’*/
SELECT complaint_id FROM complaints
WHERE tags = "Older American"

/*Buat sebuah view yang menampilkan data nama perusahaan, jumlah company response to consumer seperti tabel di bawah*/

SELECT company, SUM(untimely_response) AS "Untimely response",
		SUM(closed) AS Closed, 
		SUM(closed_exp) AS "Closed with explanation", 
		SUM(closed_non) AS "Closed with non-monetary relief"
FROM(
	SELECT company,
		CASE company_response
			WHEN "Untimely response" THEN jumlah
			ELSE 0
		END AS untimely_response,
		CASE company_response
			WHEN "Closed" THEN jumlah
			ELSE 0
		END AS closed,
		CASE company_response
			WHEN "Closed with explanation" THEN jumlah
			ELSE 0
		END AS closed_exp,
		CASE company_response
			WHEN "Closed with non-monetary relief" THEN jumlah
			ELSE 0
		END AS closed_non
	FROM(
		SELECT company,company_response,COUNT(company_response) AS jumlah
		FROM complaints
		GROUP BY company) AS p
	) AS m
GROUP BY company
ORDER BY company;
	

	
